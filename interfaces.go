package geoboard

import "context"

type Browser interface {
	// radius in metres
	GetLocalMsgs(ctx context.Context, lon, lat float64, radius, maxAgeHours int) ([]Msg, error)
}

type Poster interface {
	Post(ctx context.Context, msg Msg) error
}
