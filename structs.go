package geoboard

import "time"

type Msg struct {
	Author       string
	AuthorIPAddr string
	ReplyToIds   []string
	Lon          float64
	Lat          float64
	Content      []byte
	ContentType  string
	Text         string
	Tags         []string
	UpvoteCount  int // upvotes affect the message broadcast radius
	DistanceTo   int // metres - used only in results
	Timestamp    time.Time
}
