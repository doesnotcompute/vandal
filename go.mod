module gitlab.com/doesnotcompute/geoboard

go 1.16

require (
	github.com/alicebob/miniredis/v2 v2.14.3
	github.com/go-redis/redis/v8 v8.8.0
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.7.0
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
