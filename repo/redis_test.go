package repo

import (
	"context"
	"testing"
	"time"

	miniredis "github.com/alicebob/miniredis/v2"
	"github.com/go-redis/redis/v8"
	"github.com/stretchr/testify/assert"
	"gitlab.com/doesnotcompute/geoboard"
)

func TestGetMsgs(t *testing.T) {
	s, err := miniredis.Run()
	if err != nil {
		t.Fatal(err)
	}
	defer s.Close()
	c := redis.NewClient(&redis.Options{
		Addr: s.Addr(),
	})
	defer c.Close()
	ms := NewMsgSaver(c)
	mg := NewMsgGetter(c)
	ctx := context.Background()
	id1 := "deadbeef"
	timestamp1 := time.Date(2021, 4, 21, 0, 31, 1, 0, time.UTC)
	msg1 := geoboard.Msg{
		Author:       "author",
		AuthorIPAddr: "127.0.0.1",
		ReplyToIds:   []string{"one", "two", "three"},
		Lon:          30.317422,
		Lat:          59.950539,
		Content:      []byte("awesome content"),
		ContentType:  "image/jpeg",
		Text:         "check this out",
		Tags:         []string{"check", "awesome"},
		UpvoteCount:  1, // for the purposes of this test
		DistanceTo:   0,
		Timestamp:    timestamp1,
	}
	if err := ms.SaveMsg(ctx, id1, msg1); err != nil {
		t.Fatal(err)
	}
	timestamp2 := time.Date(2021, 4, 21, 0, 55, 1, 0, time.UTC)
	doNotWantMsg2 := geoboard.Msg{
		Author:       "somedude",
		AuthorIPAddr: "127.0.0.1",
		ReplyToIds:   []string{"xxx", "yyy", "zzz"},
		Lon:          13.361389,
		Lat:          38.115556,
		Content:      []byte("ok content"),
		ContentType:  "image/jpeg",
		Text:         "hi",
		Tags:         []string{"ayyy", "lmao"},
		UpvoteCount:  1, // for the purposes of this test
		DistanceTo:   0,
		Timestamp:    timestamp2,
	}
	id2 := "cafebabe"
	if err := ms.SaveMsg(ctx, id2, doNotWantMsg2); err != nil {
		t.Fatal(err)
	}
	gotMsgs, err := mg.GetMsgs(ctx, 30.327735, 59.888564, 10000)
	if err != nil {
		t.Fatal(err)
	}
	wantMsgs := []geoboard.Msg{
		{
			Author:       "author",
			AuthorIPAddr: "",
			ReplyToIds:   []string{"one", "two", "three"},
			Lon:          30.317424, // floats being floaty
			Lat:          59.950539,
			Content:      []byte("awesome content"),
			ContentType:  "image/jpeg",
			Text:         "check this out",
			Tags:         []string{"check", "awesome"},
			UpvoteCount:  1, // for the purposes of this test
			DistanceTo:   6917,
			Timestamp:    timestamp1,
		},
	}
	assert.Equal(t, wantMsgs, gotMsgs)
}
