package repo

const (
	keyMsgPrefix = "msg:"

	fieldMsgAuthor            = "author"
	fieldMsgAuthorIPAddr      = "author-ip-addr"
	fieldMsgContent           = "content"
	fieldMsgContentType       = "content-type"
	fieldMsgText              = "text"
	fieldMsgUpvoteCount       = "upvoteCount"
	fieldMsgTimestamp         = "timestamp"
	idxAuthorMsgsPrefix       = "idx:author:msgs:"
	idxAuthorIPAddrMsgsPrefix = "idx:author-ip-addr:msgs:"
	idxReplyToIdMsgsPrefix    = "idx:reply-to-id:msgs:"
	idxTimestampMsgs          = "idx:timestamp:msgs"
	idxTagMsgsPrefix          = "idx:tag:msgs:"

	keyMsgCoords           = "msg:coords"
	keyMsgReplyToIdsPrefix = "msg:reply-to-ids:"
	keyMsgUpvoteCount      = "msg:upvotecount"
	keyMsgTagsPrefix       = "msg:tags:"
)
