package repo

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"github.com/go-redis/redis/v8"
	"gitlab.com/doesnotcompute/geoboard"
)

type MsgGetter struct {
	c *redis.Client
}

func NewMsgGetter(c *redis.Client) MsgGetter {
	return MsgGetter{
		c: c,
	}
}

func (mg MsgGetter) GetMsgs(ctx context.Context, lon, lat float64, radius int) ([]geoboard.Msg, error) {
	handleErr := func(err error) ([]geoboard.Msg, error) {
		return nil, fmt.Errorf("redis get msgs: %w", err)
	}
	rr, err := mg.c.GeoRadius(ctx, keyMsgCoords, lon, lat, &redis.GeoRadiusQuery{
		Radius:    float64(radius),
		Unit:      "m",
		WithCoord: true,
		WithDist:  true,
		Sort:      "ASC",
	}).Result()
	if err != nil {
		return handleErr(err)
	}
	msgs := make([]geoboard.Msg, len(rr))
	for i, r := range rr {
		rawMsg, err := mg.c.HGetAll(ctx, keyMsgPrefix+r.Name).Result()
		if err != nil {
			return handleErr(err)
		}
		replyToIds, err := mg.c.LRange(ctx, keyMsgReplyToIdsPrefix+r.Name, 0, -1).Result()
		if err != nil {
			return handleErr(err)
		}
		tags, err := mg.c.LRange(ctx, keyMsgTagsPrefix+r.Name, 0, -1).Result()
		if err != nil {
			return handleErr(err)
		}
		msg, err := buildMsg(r.Longitude, r.Latitude, r.Dist, rawMsg, replyToIds, tags)
		if err != nil {
			return handleErr(err)
		}
		msgs[i] = msg
	}
	return msgs, nil
}

func buildMsg(lon, lat, dist float64, m map[string]string, replyToIds []string, tags []string) (geoboard.Msg, error) {
	upvoteCount, err := strconv.Atoi(m[fieldMsgUpvoteCount])
	if err != nil {
		return geoboard.Msg{}, err
	}
	timestamp := time.Time{}
	if err := timestamp.UnmarshalText([]byte(m[fieldMsgTimestamp])); err != nil {
		return geoboard.Msg{}, err
	}
	return geoboard.Msg{
		Author:       m[fieldMsgAuthor],
		AuthorIPAddr: "", // intentional
		ReplyToIds:   replyToIds,
		Lon:          lon,
		Lat:          lat,
		Content:      []byte(m[fieldMsgContent]),
		ContentType:  m[fieldMsgContentType],
		Text:         m[fieldMsgText],
		Tags:         tags,
		UpvoteCount:  upvoteCount,
		DistanceTo:   int(dist),
		Timestamp:    timestamp,
	}, nil
}
