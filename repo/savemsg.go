package repo

import (
	"context"
	"fmt"

	"github.com/go-redis/redis/v8"
	"gitlab.com/doesnotcompute/geoboard"
)

type MsgSaver struct {
	c *redis.Client
}

func NewMsgSaver(c *redis.Client) MsgSaver {
	return MsgSaver{
		c: c,
	}
}

func (ms MsgSaver) SaveMsg(ctx context.Context, id string, msg geoboard.Msg) error {
	handleErr := func(err error) error {
		return fmt.Errorf("redis: save msg: %w", err)
	}
	loc := redis.GeoLocation{
		Name:      id,
		Longitude: msg.Lon,
		Latitude:  msg.Lat,
		Dist:      0,
		GeoHash:   0,
	}
	if _, err := ms.c.TxPipelined(ctx, func(pipe redis.Pipeliner) error {
		// save msg coords
		_ = pipe.GeoAdd(ctx, keyMsgCoords, &loc)
		// save msg
		timestampBytes, err := msg.Timestamp.MarshalText()
		if err != nil {
			return handleErr(err)
		}
		_ = pipe.HSet(
			ctx, keyMsgPrefix+id,
			fieldMsgAuthor, msg.Author,
			fieldMsgAuthorIPAddr, msg.AuthorIPAddr,
			fieldMsgContent, msg.Content,
			fieldMsgContentType, msg.ContentType,
			fieldMsgText, msg.Text,
			fieldMsgUpvoteCount, msg.UpvoteCount,
			fieldMsgTimestamp, timestampBytes,
		)
		// save msg reply-to ids
		for _, rti := range msg.ReplyToIds {
			_ = pipe.RPush(ctx, keyMsgReplyToIdsPrefix+id, rti)
		}
		// index reply-to-ids
		for _, replyToId := range msg.ReplyToIds {
			_ = pipe.RPush(ctx, idxReplyToIdMsgsPrefix+replyToId, id)
		}
		// save msg tags
		for _, t := range msg.Tags {
			_ = pipe.RPush(ctx, keyMsgTagsPrefix+id, t)
		}
		// index msg tags
		for _, tag := range msg.Tags {
			_ = pipe.RPush(ctx, idxTagMsgsPrefix+tag, id)
		}
		// index msg author
		_ = pipe.RPush(ctx, idxAuthorMsgsPrefix+msg.Author, id)
		// index message author ip address
		_ = pipe.RPush(ctx, idxAuthorIPAddrMsgsPrefix+msg.AuthorIPAddr, id)
		// create upvote counter
		_ = pipe.ZAdd(ctx, keyMsgUpvoteCount, &redis.Z{
			Score:  float64(msg.UpvoteCount),
			Member: id,
		})
		// index msg timestamp
		_ = pipe.ZAdd(ctx, idxTimestampMsgs, &redis.Z{
			Score:  float64(msg.Timestamp.Unix()),
			Member: id,
		})
		return nil
	}); err != nil {
		return handleErr(err)
	}
	return nil
}
