package geoboard

import "errors"

var ErrNoTopicsFound = errors.New("no topics found")
