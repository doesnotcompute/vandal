package app

import (
	"context"
	"fmt"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/doesnotcompute/geoboard"
)

var _ geoboard.Poster = App{}
var _ geoboard.Browser = App{}

const (
	defaultMinRadius = 1e+4 // do be decreased as we scale
	defaultMaxRadius = 1e+6 // to be decreased as we scale
)

// MsgSaver creates inverse indices as well
type MsgSaver interface {
	SaveMsg(ctx context.Context, id string, msg geoboard.Msg) error
}

type MsgBrowser interface {
	GetMsgs(ctx context.Context, lon, lat float64, radius int) ([]geoboard.Msg, error)
}

type App struct {
	ms MsgSaver
	mb MsgBrowser
}

func (a App) Post(ctx context.Context, msg geoboard.Msg) error {
	handleErr := func(err error) error {
		return fmt.Errorf("post msg: %w", err)
	}
	id := uuid.NewV4().String()
	if err := a.ms.SaveMsg(ctx, id, msg); err != nil {
		return handleErr(err)
	}
	return nil
}

func (a App) GetLocalMsgs(ctx context.Context, lon, lat float64, radius, maxAgeHours int) ([]geoboard.Msg, error) {
	handleErr := func(err error) ([]geoboard.Msg, error) {
		return nil, fmt.Errorf("browse msgs: %w", err)
	}
	for rad := radius; radius < defaultMaxRadius; radius *= 2 {
		msgs, err := a.mb.GetMsgs(ctx, lon, lat, rad)
		if err != nil {
			return handleErr(err)
		}
		if len(msgs) > 0 {
			return msgs, nil
		}
	}
	return []geoboard.Msg{}, nil
}
